#!/bin/bash
# Author Basem Khanji: basem.khanji@cern.ch

main_dir="$(pwd)/"
repos=()

# Color variables
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

# Function to update the master branch in a Git repository
update_git_repo() {
  repo_path="$1"
  echo -e "${BLUE}Updating repository: $repo_path${NC}"

  # Change to the repository directory
  cd "$repo_path" || exit

  # Check if there are unstaged changes
  if [[ -n $(git status --porcelain) ]]; then
    echo -e "${RED}Unstaged changes detected in the repository.${NC}"
    echo -e "Please choose an option:"
    echo -e "1. ${RED}Stash changes${NC} and continue with update"
    echo -e "2. ${RED}Skip update${NC} for this repository"
    echo -e "3. ${RED}Abort${NC} the script"
    read -rp "Enter your choice (1/2/3): " choice

    case $choice in
      1)
        # Stash changes and continue with update
        git stash
        ;;
      2)
        # Skip update for this repository
        echo -e "${RED}Skipping update${NC} for repository: $repo_path"
        echo
        return
        ;;
      3)
        # Abort the script
        echo -e "${RED}Aborting the script.${NC}"
        exit
        ;;
      *)
        # Invalid choice, skip update for this repository
        echo -e "${RED}Invalid choice.${NC} Skipping update for repository: $repo_path"
        echo
        return
        ;;
    esac
  fi

  # Update the master branch
  git fetch
  git checkout master
  git pull origin master

  echo -e "${GREEN}Repository updated.${NC}"
  echo
}

# Function to collect Git repository paths within the main directory
collect_git_repo_paths() {
  local dir="$1"
  echo -e "${BLUE}Searching for Git repositories in directory: $dir${NC}"

  # Loop through each entry in the directory
  for entry in "$dir"/*; do
    if [ -d "$entry" ]; then
      if [ -d "$entry/.git" ]; then
        # It's a Git repository
        echo -e "${BLUE}Git repository found: $entry${NC}"
        repos+=("$entry")
      else
        # It's a directory, so recursively collect Git repository paths inside it
        collect_git_repo_paths "$entry"
      fi
    fi
  done
}

# Collect Git repository paths within the main directory
collect_git_repo_paths "$main_dir"

# Update the master branch for each collected repository
for repo in "${repos[@]}"; do
  update_git_repo "$repo"
done


cd "$main_dir" || exit
# Set the number of parallel jobs for make
num_cpus=$(nproc)
# Execute make with parallel jobs
echo "Found $num_cpus CPUs on your machine"
make_jobs=$((num_cpus - 1))
make + -j"$make_jobs" Moore

# Function to display a confirmation prompt
confirm() {
  while true; do
    read -rp "Do you want to execute the make command? (yes/no): " choice
    case $choice in
      [Yy]*)
        return 0
        ;;
      [Nn]*)
        return 1
        ;;
      *)
        echo "Invalid choice. Please enter 'yes' or 'no'."
        ;;
    esac
  done
}

main_dir="main"

# Prompt for confirmation
confirm || exit

# Set the number of parallel jobs for make
num_cpus=$(nproc)
make_jobs=$((num_cpus - 1))

# Execute make command in the main directory
cd "$main_dir" || exit
make -j$make_jobs Moore

