#!/bin/tcsh

set parent_directory = "`pwd`"
cd $parent_directory

foreach project_dir (`find . -maxdepth 1 -type d ! -path .`)
    cd $project_dir
    if (-d .git) then
        git checkout master
        git pull origin master
        git rebase master
    endif
    cd ..
end
